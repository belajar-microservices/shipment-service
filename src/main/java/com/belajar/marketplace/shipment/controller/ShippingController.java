/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.shipment.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author opaw
 */
@RestController
public class ShippingController {
    
    @GetMapping("/api/fare")
    public Map<String, Object> getFareResult(@RequestParam String origin, @RequestParam String dest, @RequestParam String weight){
        Map<String, Object> res = new HashMap<>();
        res.put("origin", origin);
        res.put("dest", dest);
        res.put("weight", weight);
        res.put("fare", 50000);
        
        return res;
    }
    
}
